<?php

require_once('../vnzsql/base/class.basevnz.php');
require('../vnzsql/class.odbcdb.php');

$cbd = vnzsql_odbc::singleton();

// Parametros ( DRIVER , USUARIO, CLAVE, BASE DE DATOS, PUERTO, PERSISTENCIA )

$cbd->conectar('DRIVER={MySQL ODBC 5.2 Unicode Driver};Server=localhost;Database=vnzsql_pruebas','root','');

$cbd->set_tiporespuesta(VNZ_RES_ASSOC);

//print_r($cbd->select('usuarios'));

//var_dump($cbd->select('usuarios','id,nombre',NULL,'nombre="Misael Black"',' id DESC '));
//var_dump($cbd->delete('usuarios', ' id = 3 '));
//var_dump($cbd->update('usuarios', array('email'=>"nuevo@email.com"), ' id="2" AND nombre="Pedro Fernandez" '));

// uso de insert y escape

//$datos = array('nombre' => 'Misa3l', 'email' => 'misa@email.com', 'direccion' => 'venezuela');
//$datos = $cbd->escape($datos);
//var_dump($cbd->insert('usuarios', $datos));

//var_dump($cbd->get_variable("SELECT nombre FROM usuarios WHERE direccion = 'venezuela'"));
//var_dump($cbd->get_variable("SELECT id,nombre FROM usuarios WHERE direccion = 'venezuela'",1));

/*
$cbd->query("select * from usuarios");
while($row = $cbd->get_siguiente()){
	print_r($row);
}
*/

//$cbd->query("select * from usuarios");
//print_r($cbd->get_tablas());

//echo $cbd->escape(" misael' ");

//$cbd->query("select * from usuarios");
//print_r($cbd->get_nombrecolumnas());

//$cbd->query("select * from usuarios");
//print_r($cbd->get_columnas());

//echo $cbd->get_identificador();

//echo $cbd->get_dbtipo(); 

//echo $cbd->get_version();

//$valor = $cbd->get_uno("SELECT * FROM usuarios WHERE direccion = 'venezuela'");
//echo $valor->nombre;

//$cbd->query("SELECT * FROM usuarios");
//print_r($cbd->get_siguiente());

//$cbd->query("SELECT * FROM usuarios");
//$array = $cbd->get_siguiente();
//$objeto = $cbd->array2obj($array);
//echo $objeto->nombre;

//$cbd->query("SELECT * FROM usuarios");
//print_r($cbd->get_todo());

//$cbd->query("SELECT * FROM usuarios WHERE email = 'blacksecured@gmail.com'");
//$row = $cbd->get_siguiente();
//echo "Usuario: ".$row["nombre"]."<br />";
//echo "Direccion: ".$row["direccion"]."<br />";

//$cbd->query("SELECT * FROM usuarios");
//echo $cbd->rows_afectados();

/*
$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
if($cbd->numero_rows() > 0){
    echo "bien";
}else{
    echo "nada";
}
*/

//$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
//echo $cbd->numero_rows();

//$cbd->query("SELECT * FROM usuarios");
//echo $cbd->numero_rows();

//$cbd->liberar();

$cbd->desconectar();

unset($cbd);

?>