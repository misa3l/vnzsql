<?php

require_once('../vnzsql/base/class.basevnz.php');
require('../vnzsql/class.pdodb.php');

$cbd = vnzsql_pdo::singleton();

$cbd->set_dbtipo("mysql");
//$cbd->set_dbtipo("postgres");
//$cbd->set_dbtipo("sqlite3");

$cbd->conectar("localhost", "root", "", "vnzsql_pruebas", 3306);
//$cbd->conectar("localhost", "vnzsql_user", "1234", "vnzsql_pruebas", 5432);
//$cbd->conectar(/etc/pastechare/vnzsql.db");

//$cbd->set_tiporespuesta(VNZ_RES_ASSOC);
//$cbd->set_charset("utf8");

//print_r($cbd->select('usuarios'));

//var_dump($cbd->select('usuarios','id,nombre',NULL,'nombre="Misael Black"',' id DESC '));
//var_dump($cbd->delete('usuarios', ' id = 3 '));
//var_dump($cbd->update('usuarios', array('email'=>"nuevo@email.com"), ' id="2" AND nombre="Pedro Fernandez" '));

// uso de insert y escape

//$datos = array('nombre' => 'Misa3l', 'email' => 'misa@email.com', 'direccion' => 'venezuela');
//$datos = $cbd->escape($datos);
//var_dump($cbd->insert('usuarios', $datos));

//var_dump($cbd->get_variable("SELECT id,nombre FROM usuarios WHERE direccion = 'venezuela'",1));

/*
$cbd->query("select * from usuarios where nombre = 'Misael Black' ");
$cbd->query("select * from usuarios");

while($row = $cbd->get_siguiente()){
    print_r($row);
}

print_r($cbd->get_database());
print_r($cbd->get_tablas());
print_r($cbd->get_siguiente());
print_r($cbd->get_todo());
print_r($cbd->rows_afectados());
print_r($cbd->numero_rows());
print_r($cbd->fobject());
print_r($cbd->get_uno("select * from usuarios where nombre = 'Pedro Fernandez' "));
print_r($cbd->get_nombrecolumnas());
var_dump($cbd->liberar());
*/

//echo $cbd->escape(" hola' ");

$cbd->desconectar();

unset($cbd);


?>