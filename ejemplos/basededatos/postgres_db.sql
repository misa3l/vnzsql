-- CREATE ROLE vnzsql_user LOGIN ENCRYPTED PASSWORD '1234' NOINHERIT VALID UNTIL 'infinity';
-- CREATE DATABASE vnzsql_pruebas WITH ENCODING='UTF8' OWNER=vnzsql_user TEMPLATE template0;
-- \q
-- psql vnzsql_pruebas
-- \i /tmp/database.sql

drop table usuarios;

CREATE TABLE IF NOT EXISTS usuarios (
    id integer NOT NULL,
    nombre character varying(150) NOT NULL,
    email character varying(150) NOT NULL,
    direccion character varying(150) NOT NULL
);

ALTER TABLE public.usuarios OWNER TO vnzsql_user;

INSERT INTO usuarios VALUES (1, 'Misael Black', 'blacksecured@gmail.com', 'venezuela');
INSERT INTO usuarios VALUES (2, 'Pedro Fernandez', 'pedrito@correo.com', 'panama');
INSERT INTO usuarios VALUES (3, 'Carlos Jojoto', 'jojoto@email.com', 'ecuador');

