<?php

require_once('../vnzsql/base/class.basevnz.php');
require('../vnzsql/class.mysqldb.php');

$cbd = vnzsql_mysql::singleton();

// host usuario password database puerto
$cbd->conectar("localhost", "root", "", "vnzsql_pruebas", 3306);
$cbd->set_charset("utf8");

$cbd->set_tiporespuesta(VNZ_RES_ASSOC);

// cache de query usando disco duro
$cbd->cache_disco = true;                    // flag de cache disco TRUE
$cbd->cache = true;                          // flag del cache TRUE
$cbd->tiempo_cache = 24;                     // horas
$cbd->set_cache_extension("vnzsql_cache");   // extensión
$cbd->cache_dir = 'vnzsql_cache';            // directorio

// cache de query usando memcache, cache en memoria
/*
$cbd->cache_disco = false;                   // flag de cache disco FALSE
$cbd->cache = true;                          // flag del cache TRUE
$cbd->tiempo_cache = 24;                     // horas
$cbd->memcache_host = '127.0.0.1';           // host memcache
$cbd->memcache_puerto = 11211;               // puerto memcache
*/

//print_r($cbd->select('usuarios'));
//var_dump($cbd->select('usuarios','id,nombre',NULL,'nombre="Misael Black"',' id DESC '));
//var_dump($cbd->delete('usuarios', ' id = 3 '));
//var_dump($cbd->update('usuarios', array('email'=>"nuevo@email.com"), ' id="2" AND nombre="Pedro Fernandez" '));

// uso de insert y escape

//$datos = array('nombre' => 'Misa3l', 'email' => 'misa@email.com', 'direccion' => 'venezuela');
//$datos = $cbd->escape($datos);
//var_dump($cbd->insert('usuarios', $datos));

// Inicio del tiempo
$cbd->tiempoquery();

// Podemos hacer cache de los métodos get_uno | execute | get_variable

var_dump($cbd->get_uno("select * from usuarios"));
//var_dump($cbd->execute("select * from usuarios"));
//var_dump($cbd->get_variable("SELECT nombre FROM usuarios WHERE direccion = 'venezuela'"));

// Fin del tiempo
echo $cbd->tiempoquery();

// por defecto 
//print_r($cbd->query_cache("select * from usuarios",'get_siguiente'));

//var_dump($cbd->query_cache("SELECT nombre FROM usuarios WHERE direccion = 'venezuela'",'get_variable'));

//print_r($cbd->query_cache("select * from usuarios",'get_uno',VNZ_RES_ASSOC));

//print_r($cbd->query_cache("select * from usuarios",'get_siguiente',VNZ_RES_ASSOC));

//print_r($cbd->query_cache("select * from usuarios",'get_todo',VNZ_RES_ASSOC));

$cbd->liberar();

$cbd->desconectar();

unset($cbd);

?>