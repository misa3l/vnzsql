<?php

require_once('../vnzsql/base/class.basevnz.php');
require('../vnzsql/class.postgresdb.php');

$cbd = vnzsql_postgres::singleton();
$cbd->conectar("localhost", "vnzsql_user", "1234", "vnzsql_pruebas", 5432);
$cbd->set_charset("utf8");

//$cbd->set_tiporespuesta(VNZ_RES_ASSOC);

//print_r($cbd->select('usuarios'));

//var_dump($cbd->select('usuarios','id,nombre',NULL,'nombre="Misael Black"',' id DESC '));
//var_dump($cbd->delete('usuarios', ' id = 3 '));
//var_dump($cbd->update('usuarios', array('email'=>"nuevo@email.com"), ' id="2" AND nombre="Pedro Fernandez" '));

// uso de insert y escape

//$datos = array('nombre' => 'Misa3l', 'email' => 'misa@email.com', 'direccion' => 'venezuela');
//$datos = $cbd->escape($datos);
//var_dump($cbd->insert('usuarios', $datos));

//var_dump($cbd->get_variable("SELECT id,nombre FROM usuarios WHERE direccion = 'venezuela'",1));

/*
echo $cbd->escape(" misael' ");
SALIDA:
misael'' 

$cbd->query("select * from usuarios");
print_r($cbd->get_nombrecolumnas());
SALIDA:
Array
(
    [0] => id
    [1] => nombre
    [2] => email
    [3] => direccion
)

$cbd->query("select * from usuarios");
print_r($cbd->get_columnas());
SALIDA:
Array
(
    [0] => Array
        (
            [id] => 1
            [nombre] => Misael Black
            [email] => blacksecured@gmail.com
            [direccion] => venezuela
        )

    [1] => Array
        (
            [id] => 2
            [nombre] => Pedro Fernandez
            [email] => pedrito@correo.com
            [direccion] => panama
        )

    [2] => Array
        (
            [id] => 3
            [nombre] => Carlos Jojoto
            [email] => jojoto@email.com
            [direccion] => ecuador
        )

)

echo $cbd->get_identificador();
SALIDA: 
POSTGRES

echo $cbd->get_dbtipo(); 
SALIDA:
postgres

echo $cbd->get_version();
SALIDA:
VNZSQL: 0.1

$rs = $cbd->query("INSERT INTO usuarios (id,nombre,email,direccion) VALUES (4,'manuel','manuel@jmail.com','chile')");
if($rs){
    echo "Insertado Con exito";
}else{
    echo "Error al insertar?";
    echo $cbd->error;
}

$valor = $cbd->get_uno("SELECT * FROM usuarios WHERE direccion = 'venezuela'");
echo $valor->nombre;
SALIDA:
Misael Black

$cbd->query("SELECT * FROM usuarios");
print_r($cbd->get_siguiente());
SALIDA:
Array
(
    [id] => 1
    [nombre] => Misael Black
    [email] => blacksecured@gmail.com
    [direccion] => venezuela
)

$cbd->query("SELECT * FROM usuarios");
$array = $cbd->get_siguiente();
$objeto = $cbd->array2obj($array);
echo $objeto->nombre;
SALIDA:
Misael Black

$cbd->query("SELECT * FROM usuarios");
print_r($cbd->get_todo());
SALIDA:
Array
(
    [0] => Array
        (
            [id] => 1
            [nombre] => Misael Black
            [email] => blacksecured@gmail.com
            [direccion] => venezuela
        )

    [1] => Array
        (
            [id] => 2
            [nombre] => Pedro Fernandez
            [email] => pedrito@correo.com
            [direccion] => panama
        )

    [2] => Array
        (
            [id] => 3
            [nombre] => Carlos Jojoto
            [email] => jojoto@email.com
            [direccion] => ecuador
        )

)

$cbd->query("SELECT * FROM usuarios WHERE email = 'blacksecured@gmail.com'");
$row = $cbd->get_siguiente();
echo "Usuario: ".$row["nombre"]."<br />";
echo "Direccion: ".$row["direccion"]."<br />";
SALIDA:
Usuario: Misael Black
Direccion: venezuela

$cbd->query("SELECT * FROM usuarios");
echo $cbd->rows_afectados();
SALIDA:
3

$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
if($cbd->numero_rows() > 0){
    echo "bien";
}else{
    echo "nada";
}

$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
echo $cbd->numero_rows();
SALIDA:
1

$cbd->query("SELECT * FROM usuarios");
echo $cbd->numero_rows();
SALIDA:
3

// aun no funciona get_tablas() ( en desarrollo )
$cbd->query("select * from usuarios");
print_r($cbd->get_tablas());

*/

//$cbd->liberar();

$cbd->desconectar();

unset($cbd);

?>