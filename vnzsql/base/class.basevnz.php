<?php
/* vnzsql/base/class.basevnz.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://gitlab.com/misa3l/vnzsql .
* Licenciado bajo la AGPL versión 3 o superior .
*/

require_once('class.astractvnz.php');

if ( ! class_exists ('ClassAstractDB') ) die('<b>Error Fatal:</b> ClassBaseVNZ Requiere incluir (class.astractvnz.php) Ejemplo: require_once(\'class.astractvnz.php\'); ');

/**
 * Componente Base (VNZSQL database abstraction library)
 *
 * @package ClassBaseVNZ
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 2.1
 * @access public
 */

class ClassBaseVNZ extends ClassAstractDB {

    /**
    * Tiempo de Cache en horas
    * @var integer
    * @access public
    */
    public $tiempo_cache = 24;

    /**
    * Directorio de Cache
    * @var string
    * @access public
    */
    public $cache_dir = 'vnzsql_cache';

    /**
    * Flag para el cache
    * @var boolean
    * @access public
    */
    public $cache = false;
    
    /**
     * Cache en disco
     * @var boolean
     * @access public 
     */
    public $cache_disco = false;
    
    /**
     * Instancia de Memcache
     * @var object | boolean
     * @access private
     */
    private static $mem_instancia = false;
    
    /**
     * Flag memcache
     * @var boolean
     * @access public
     */
    public $memcache = false;
    
    /**
     * Flag comprecion memcache
     * @var boolean 
     * @access public
     */
    public $memcache_compressed = false;
    
    /**
     * Host memcache
     * @var string
     * @access public
     */
    public $memcache_host = '127.0.0.1';
    
    /**
     * Puerto memcache
     * @var integer
     * @access public
     */
    public $memcache_puerto = 11211;
    
    /**
    * Extensión del cache
    * @var string
    * @access public
    */
    public $extension_cache = 'vnzsql_cache';

    /**
     * Tipo de base de datos
     * @var string
     * @access private
     */
    public $dbtipo;

    /**
     * Método Constructor Base
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo, $prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        $this->dbtipo = $dbtipo;
        $this->_id = "Clase base abstracta";
    }

    /**
     * Método conectar Base inicializa las propiedades del Método conectar
     * @param string $host
     * @param string $usuario
     * @param string $clave
     * @param string $basedatos
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host, $usuario, $clave, $basedatos, $puerto, $persistente = FALSE) {
        $this->host = $host;
        $this->usuario = $usuario;
        $this->clave = $clave;
        $this->basedatos = $basedatos;
        $this->puerto = $puerto;
        $this->persistente = $persistente;
        return TRUE;
    }

    /**
     * Método set_dbtipo Base Envió el tipo de Base de datos
     * @param string $dbtipo
     */
    public function set_dbtipo($dbtipo) {
        $this->dbtipo = $dbtipo;
    }
    
    /**
     * Método query Base guarda el ultimo query y liberar memoria
     * @param string $query
     */
    public function query($query) {
        $this->ultimoQuery = $query;
        $this->error = '';
        $this->liberar();
    }

    /**
     * Método get_ultimoquery Base retorna el ultimo query ejecutado
     * @return string
     */
    public function get_ultimoquery() {
        parent::get_ultimoquery();
        return $this->ultimoQuery;
    }

    /**
     * Método get_todo Base retorna un array completo de una consulta
     * @param string $resultadoTipo
     * @return array
     */
    public function get_todo($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $res = array();
        while ($col = $this->get_siguiente($resultadoTipo)) {
            array_push($res, $col);
        }
        return $res;
    }

    /**
     * Método get_variable retornara un string único
     * @param string $query
     * @param string $resultadoTipo
     * @param integer $posicion
     */
    public function get_variable($query, $resultadoTipo = VNZ_VALOR_PREDEFINIDO, $posicion = 0){
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO)
        {
            $resultadoTipo = $this->prefResTipo;
        }
        if($this->cache)
        {
            return $this->query_cache($query,'get_variable',$resultadoTipo);
        }
        if($this->query($query)){
            $valores = @array_values($this->get_siguiente($resultadoTipo));
        }
        return (isset($valores[$posicion]) && $valores[$posicion] !== '') ? $valores[$posicion]:null;
    }
    
    /**
     * Método execute Base ejecuta una query y retorna un array completo
     * @param string $query
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function execute($query, $resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO)
        {
            $resultadoTipo = $this->prefResTipo;
        }
        if($this->cache)
        {
            return $this->query_cache($query,'get_todo',$resultadoTipo);
        }
        if ($this->query($query)) {
            return $this->get_todo($resultadoTipo);
        } else {
            return FALSE;
        }
    }
    
    /**
     * Método tablaExiste verifica si el nombre de una tabla existe en la db
     * @param string $tabla
     * @return boolean
     */
    private function tablaExiste($tabla){
        $rs = $this->query('SHOW TABLES FROM '.$this->basedatos.' LIKE "'.$tabla.'"');
        if($rs){
            if($this->numero_rows() > 0){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Método select Base crud facilita el uso de SELECT
     * @param string $tabla
     * @param string $rows
     * @param string $join
     * @param string $where
     * @param string $order
     * @param string $limit
     * @return boolean
     */
    public function select($tabla, $rows = '*', $join = null, $where = null, $order = null, $limit = null) {
        
        if($this->tablaExiste($tabla)){
            
            $q = 'SELECT '.$rows.' FROM '.$tabla;
            
            if($join != null)
            {
                $q .= ' JOIN '.$join;
            }
            if($where != null)
            {
                $q .= ' WHERE '.$where;
            }
            if($order != null)
            {
                $q .= ' ORDER BY '.$order;
            }
            if($limit != null)
            {
                $q .= ' LIMIT '.$limit;
            }
            
            $query = $this->query($q);
            if($query)
            {
                //return $this->get_todo($this->prefResTipo);
                return true;
            }else
            {
                return false;
            }
        }else{
            return false;
        }
    }

	/**
     * Método update Base crud facilita el uso de UPDATE
     * @param string $tabla
     * @param array $params
     * @param string $where
     * @return boolean
     */
    public function update($tabla, $params = array(), $where){

        if($this->tablaExiste($tabla)){

            $args=array();
            foreach($params as $fila=>$value)
            {
                $args[] = $fila.'="'.$value.'"';
            }
            $q = 'UPDATE '.$tabla.' SET '.implode(',',$args).' WHERE '.$where;
            $query = $this->query($q);
            if($query)
            {
                return true;
            }else
            {
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Método insert Base crud facilita el uso de INSERT
     * @param string $tabla
     * @param array $params
     * @return boolean
     */
    public function insert($tabla, $params = array()){

        if($this->tablaExiste($tabla)){

            $q = 'INSERT INTO `'.$tabla.'` (`'.implode('`, `',array_keys($params)).'`) VALUES ("' . implode('", "', $params) . '")';
            $query = $this->query($q);
            if($query)
            {
                return true;
            }else
            {
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Método delete Base crud facilita el uso de DELETE
     * @param string $tabla
     * @param string $where
     * @return boolean
     */
    public function delete($tabla, $where = null){

        if($this->tablaExiste($tabla)){

            if($where == null)
            {
                $q = 'DELETE '.$tabla;
            }else
            {
                //$q = 'DELETE FROM '.$tabla.' WHERE '.$where;
                $q = "DELETE FROM ".$tabla." WHERE ".$where;
            }
            $query = $this->query($q);
            if($query)
            {
                return true;
            }else
            {
                return false;
            }
        }else{
            return false;
        }
    }
    
    /**
     * Método query_cache Base ejecuta una query y guarda su cache en disco o en memcache
     * @param string $query
     * @param string $modo_query
     * @param string $resultadoTipo
     * @return boolean|array|object
     */
    public function query_cache($query, $modo_query = 'get_siguiente' , $resultadoTipo = VNZ_VALOR_PREDEFINIDO){
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO)
        {
            $resultadoTipo = $this->prefResTipo;
        }
        if(!$this->cache)
        {
            $this->error = "Flag Cache FALSE";
            return false;
        }
        
        if(!$this->cache_disco && $this->cache == true)
        {
            if(!isset($this->memcache_host))
            {
                $this->error = "Error Memcache Host " . PHP_EOL ;
                return false;
            }
            
            if(!isset($this->memcache_puerto))
            {
                $this->error = "Error Memcache Puerto " . PHP_EOL;
                return false;
            }
            
            if(!class_exists("Memcache"))
            {
                die("<b>Error Fatal:</b> Requiere tener habilitado la Librería Memcache");
            }
            
            if(null === self::$mem_instancia) {
			self::$mem_instancia = new Memcache();
            }
			
            if(!self::$mem_instancia)
            {
                $this->error = "Error Memcache FALSE " . PHP_EOL;
                return false;
            }
            
            $rs = self::$mem_instancia->connect($this->memcache_host, $this->memcache_puerto);
            
            if(!$rs)
            {
                $this->error = "Memcache Connect FALSE " . PHP_EOL;
				unset($rs);
                return false;
            }        
            unset($rs);
        }
        
        if(!is_dir($this->cache_dir))
        {
            $this->error = "Error al Abrir: ".$this->cache_dir;
            return false;
        }
        switch ($modo_query) {
            case 'get_siguiente':
                
                if(!$this->cache_disco && $this->cache == true)
                {
                    
                    $key = 'get_siguiente_cache_'.$query.'_'.$resultadoTipo;
                    $contenido = self::$mem_instancia->get(md5($key));
                    
                    if($contenido == false)
                    {
                        if($this->query($query))
                        {
                            $this->resultado_cache = $this->get_siguiente($resultadoTipo);
                            self::$mem_instancia->set(md5($key), $this->resultado_cache, $this->memcache_compressed, $this->tiempo_cache*3600);
                            self::$mem_instancia->close();
                            return $this->resultado_cache;
                        }else
                        {
                            $this->error = "Query get_siguiente MemCache FALSE";
                            return false;
                        }
                        
                    }else
                    {
                        $contenido = self::$mem_instancia->get(md5($key));
                        self::$mem_instancia->close();
                        return $contenido;
                    }
                    
                }else
                {
                
                    $archivo_cache = $this->cache_dir.'/'.md5('get_siguiente_cache_'.$query.'_'.$resultadoTipo).'.'.$this->extension_cache;
                    if (file_exists($archivo_cache))
                    {
                         return $this->_get_cache_file($archivo_cache,$query,$modo_query,$resultadoTipo);
                    }else
                    {
                        if($this->query($query))
                        {
                            /*
                            $this->resultado_cache = array(
                                'numero_rows'       => $this->numero_rows(),
                                'query_resultado'   => $this->get_siguiente($resultadoTipo)
                            );*/
                            $this->resultado_cache = $this->get_siguiente($resultadoTipo);
                            file_put_contents($archivo_cache, serialize($this->resultado_cache));
                            return $this->resultado_cache;
                        }else
                        {
                            $this->error = "Query get_siguiente Cache FALSE";
                            return false;
                        }
                    }
                }
                
                break;

            case 'get_todo':
                
                if(!$this->cache_disco && $this->cache == true)
                {
                    
                    $key = 'get_todo_cache_'.$query.'_'.$resultadoTipo;
                    $contenido = self::$mem_instancia->get(md5($key));
                    
                    if($contenido == false)
                    {
                        if($this->query($query))
                        {
                            $this->resultado_cache = $this->get_todo($resultadoTipo);
                            self::$mem_instancia->set(md5($key), $this->resultado_cache, $this->memcache_compressed, $this->tiempo_cache*3600);
                            self::$mem_instancia->close();
                            return $this->resultado_cache;
                        }else
                        {
                            $this->error = "Query get_todo MemCache FALSE";
                            return false;
                        }
                        
                    }else
                    {
                        $contenido = self::$mem_instancia->get(md5($key));
                        self::$mem_instancia->close();
                        return $contenido;
                    }
                    
                }else
                {
                
                    $archivo_cache = $this->cache_dir.'/'.md5('get_todo_cache_'.$query.'_'.$resultadoTipo).'.'.$this->extension_cache;
                    if (file_exists($archivo_cache))
                    {
                         return $this->_get_cache_file($archivo_cache,$query,$modo_query,$resultadoTipo);
                    }else
                    {
                        if($this->query($query))
                        {
                            $this->resultado_cache = $this->get_todo($resultadoTipo);
                            file_put_contents($archivo_cache, serialize($this->resultado_cache));
                            return $this->resultado_cache;
                        }else
                        {
                            $this->error = "Query get_todo Cache FALSE";
                            return false;
                        }
                    }
                }
                
                break;

                case 'get_uno':
                
                if(!$this->cache_disco && $this->cache == true)
                {
                    
                    $key = 'get_uno_cache_'.$query.'_'.$resultadoTipo;
                    $contenido = self::$mem_instancia->get(md5($key));
                    
                    if($contenido == false)
                    {
                        if($this->query($query))
                        {
                            $this->resultado_cache = (object)$this->get_siguiente($resultadoTipo);
                            self::$mem_instancia->set(md5($key), $this->resultado_cache, $this->memcache_compressed, $this->tiempo_cache*3600);
                            self::$mem_instancia->close();
                            return $this->resultado_cache;
                        }else
                        {
                            $this->error = "Query get_uno MemCache FALSE";
                            return false;
                        }
                        
                    }else
                    {
                        $contenido = self::$mem_instancia->get(md5($key));
                        self::$mem_instancia->close();
                        return $contenido;
                    }
                    
                }else
                {
                
                    $archivo_cache = $this->cache_dir.'/'.md5('get_uno_cache_'.$query.'_'.$resultadoTipo).'.'.$this->extension_cache;
                    if (file_exists($archivo_cache))
                    {
                         return $this->_get_cache_file($archivo_cache,$query,$modo_query,$resultadoTipo);
                    }else
                    {
                        if($this->query($query))
                        {
                            $get_uno = $this->get_siguiente($resultadoTipo);
                            $obj = (object)$get_uno;
                            file_put_contents($archivo_cache, serialize($obj));
                            return $obj;
                        }else
                        {
                            $this->error = "Query get_uno Cache FALSE";
                            return false;
                        }
                    }
                }
                
                break;

                case 'get_variable':
                
                if(!$this->cache_disco && $this->cache == true)
                {
                    
                    $key = 'get_variable_cache_'.$query.'_'.$resultadoTipo;
                    $contenido = self::$mem_instancia->get(md5($key));
                    
                    if($contenido == false)
                    {
                        if($this->query($query))
                        {
                            $valores = @array_values($this->get_siguiente($resultadoTipo));
                            if(isset($valores[0]) && $valores[0] !== '')
                            {
                                self::$mem_instancia->set(md5($key), $valores[0], $this->memcache_compressed, $this->tiempo_cache*3600);
                                self::$mem_instancia->close();
                                return $valores[0];
                            }else
                            {
                                $this->error = "get_variable MemCache valor[0] NULL";
                                return false;
                            }
                        }else
                        {
                            $this->error = "Query get_variable MemCache FALSE";
                            return false;
                        }
                        
                    }else
                    {
                        $contenido = self::$mem_instancia->get(md5($key));
                        self::$mem_instancia->close();
                        return $contenido;
                    }
                    
                }else
                {
                    
                    $archivo_cache = $this->cache_dir.'/'.md5('get_variable_cache_'.$query.'_'.$resultadoTipo).'.'.$this->extension_cache;
                    if (file_exists($archivo_cache))
                    {
                         return $this->_get_cache_file($archivo_cache,$query,$modo_query,$resultadoTipo);
                    }else
                    {
                        if($this->query($query))
                        {
                            $valores = @array_values($this->get_siguiente($resultadoTipo));
                            if(isset($valores[0]) && $valores[0] !== '')
                            {
                                file_put_contents($archivo_cache, serialize($valores[0]));
                                return $valores[0];
                            }else
                            {
                                $this->error = "get_variable valor[0] NULL";
                                return false;
                            }
                        }else
                        {
                            $this->error = "Query get_variable Cache FALSE";
                            return false;
                        }
                    }
                }
                
                break;

            default:
                $this->error = "query_cache Parámetro invalido";
                return false;
                break;
        }
    }

    /**
     * Método set_cache_host Envia el host a memcache
     * @param string $host
     */
    public function set_cache_host($host){
        $this->memcache_host = $host;
    }
    
    /**
     * Método get_cache_host Retorna el host de memcache
     * @return string
     */
    public function get_cache_host(){
        return $this->memcache_host;
    }
    
    /**
     * Método set_cache_puerto Envia el puerto a memcache
     * @param integer $puerto
     */
    public function set_cache_puerto($puerto){
        $this->memcache_host = $puerto;
    }
    
    /**
     * Método get_cache_puerto Retorna el puerto de memcache
     * @return integer
     */
    public function get_cache_puerto(){
        return $this->memcache_puerto;
    }
    
    /**
     * Método set_cache_extension Asigna la extensión al cache
     * @param string $extension_cache
     */
    public function set_cache_extension($extension_cache){
        $this->extension_cache = $extension_cache;
    }

    /**
     * Método get_cache_extension retorna la extensión del cache
     * @return string
     */
    public function get_cache_extension(){
        return $this->extension_cache;
    }

    /**
     * Método _get_cache_file Base retorna el unserialize de una consulta
     * @param string $archivo_cache
     * @param string $query
     * @param string $modo_query
     * @param string $resultadoTipo
     * @return string
     */
    private function _get_cache_file($archivo_cache,$query,$modo_query,$resultadoTipo){
        if ( (time() - filemtime($archivo_cache)) > ($this->tiempo_cache*3600) )
        {
            unlink($archivo_cache);
            return $this->query_cache($query,$modo_query,$resultadoTipo);
        }else
        {
            $resultado_cache = unserialize(file_get_contents($archivo_cache));
            return $resultado_cache;
        }
    }

    /**
     * Método get_columnas Base retorna un array con todos sus columnas y valores
     * @param string $resultadoTipo
     * @return array
     */
    public function get_columnas($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $array = $this->get_todo($resultadoTipo);
        $res = array();

        for ($i = 0; $i < sizeof($array); $i++) {
            $value = $this->_fixtipo($array[$i], $resultadoTipo);
            if ($value != false) {
                $res[$i] = $value;
            }
        }
        return $res;
    }

    /**
     * Método liberar Base retorna true
     * @return boolean
     */
    public function liberar() {
        return TRUE;
    }

    /**
     * Método get_dbtipo Base retorna el tipo de base de datos
     * @return string
     */
    public function get_dbtipo() {
        return $this->dbtipo;
    }

    /**
     * Método get_version Base retorna la Versión de VNZSQL
     * @return string
     */
    public function get_version() {
        return "VNZSQL: " . VNZ_VERSION;
    }

    /**
     * Método get_identificador Base retorna el identificador usado
     * @return string
     */
    public function get_identificador() {
        return $this->_id;
    }

    /**
     * Método get_database Base retorna el nombre de las base de datos
     * @return boolean|array
     */
    public function get_database() {
        parent::get_database();
        $res = array();
        switch ($this->get_dbtipo()) {
            // mysql
            case 'mysql':
            case 'mysqli':
            case 'mysqll':
            case 'mysqlt':
                $this->query('SHOW DATABASES');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
            // postgresql
            case 'pgsql':
            case 'postgres':
            case 'postgres64':
            case 'postgres7':
            case 'postgres9':
                $this->query('select datname as name from pg_database');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
            case 'sqlite3';
                $this->query('SELECT name FROM sys.Databases');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
                break;

            default:
                $this->error = 'Comando Desconocido!!';
                return FALSE;
        }
        return $this->get_tablas();
    }
    
    /**
     * Método get_tablas Base retorna las tablas de una base de datos
     * @return boolean|array
     */
    public function get_tablas() {
        $tabl = array();
        switch ($this->get_dbtipo()) {
            // mysql
            case 'mysql':
            case 'mysqli':
            case 'mysqll':
            case 'mysqlt':
                $this->query('show tables');
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
            // postgresql
            case 'pgsql':
            case 'postgres':
            case 'postgres64':
            case 'postgres7':
            case 'postgres9':
                $this->query('select tablename from pg_tables where tableowner = current_user');
            //$this->query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'");
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
            //odbc
            case 'odbc';
                $this->resultado = @odbc_tables($this->db);
                if ($this->resultado) {
                    foreach ($this->get_todo() as $entrada) {
                        if ($entrada['TABLE_TYPE'] != 'SYSTEM TABLE') {
                            array_push($tabl, $entrada['TABLE_NAME']);
                         }
                    }
                    return $tabl;
                }
                break;
            //sqlite3
            case 'sqlite3';
                $this->query("SELECT name FROM sqlite_master WHERE type = 'table' ");
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
                break;

            default:
                $this->error = 'Comando Desconocido!!';
                return FALSE;
        }
        return $this->get_columnas();
    }

    /**
     * Método escape Base retorna una cadena o array escapada
     * @param string|array $str
     * @return string|array
     */
    public function escape($str) {
        if (is_array($str)) {
            $res = array();
            foreach ($str as $key => $valor) {
                $res[$key] = $this->escape($valor);
            }
            return $res;
        } else {
            switch ($this->dbtipo) {
                // mysql
                case 'mysql':
                case 'mysqll':
                case 'mysqlt':
                    return mysql_real_escape_string(stripslashes($str));
                case 'mysqli':
                    return mysqli_real_escape_string($this->db, stripslashes($str));
                // postgresql
                case 'pgsql':
                case 'postgres':
                case 'postgres64':
                case 'postgres7':
                case 'postgres8':
                case 'postgres9':
                    return pg_escape_string(stripslashes($str));
                case 'odbc':
                    return $this->escapa_string($str);
                case 'sqlite3':
                    return trim(htmlentities(SQLite3::escapeString($str)));
                    break;
            }
            return '';
        }
    }

    /**
    * Método escapa_string en bruto
    * @param string $data
    * @return string
    */
    public function escapa_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
        $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }
    
    /**
     * Método _AntesDesconectar Base inicializa todo por defecto
     * @return boolean
     */
    public function _AntesDesconectar() {
        $this->db = null;
        $this->host = '';
        $this->database = '';
        $this->usuario = '';
        $this->clave = '';
        $this->puerto = 0;
        return true;
    }
    
    /**
     * Método array2obj Base transforma un array en un objeto
     * @param array $array
     * @return boolean|object
     */
    public function array2obj($array) {
        parent::array2obj($array);
        if (is_array($array)) {
            $obj = (object)$array;
            return $obj;
        } else {
            $this->error = "array2obj array FALSE";
            return false;
        }
    }

    /**
     * Método obj2array Base transforma un objeto en un array
     * @param array $array
     * @return boolean|array
     */
    public function obj2array($obj) {
        parent::obj2array($obj);
        if (is_object($obj)) {
            $array = array();
            $array = (array)$obj;
            return $array;
        } else {
            $this->error = "obj2array array FALSE";
            return false;
        }
    }

    /**
     * Método tiempoquery Base Usado para medir el tiempo de una consulta
     * @return string
     */
    public function tiempoquery() {
        parent::tiempoquery();
        static $_tiempo_query_inicio;
        list($usec, $sec) = explode(' ',microtime());
        if(!isset($_tiempo_query_inicio))
        {
            $_tiempo_query_inicio = ((float)$usec + (float)$sec);
        }
        else
        {
            $tiempo_query = (((float)$usec + (float)$sec)) - $_tiempo_query_inicio;
            return sprintf('<br />La consulta tard&oacute; %01.5f segundos. <br />', $tiempo_query);
        }
    }
    
    /**
     * Método _fixtipo Base
     * @param string $res
     * @param string $resultadoTipo
     * @return string
     */
    private function _fixtipo($res, $resultadoTipo = VNZ_RES_NUM) {
        $temp = @array_values($res);
        $cont = 1;
        if ($resultadoTipo == VNZ_RES_AMBAS) {
            $cont += 1;
        }
        if (sizeof($temp) == $cont) {
            return $temp[0];
        } else {
            return $res;
        }
    }

}

?>