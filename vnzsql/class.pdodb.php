<?php
/* vnzsql/class.pdodb.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://gitlab.com/misa3l/vnzsql .
* Licenciado bajo la AGPL versión 3 o superior .
*/

if ( ! class_exists ('PDO') ) die('<b>Error Fatal:</b> vnzsql_pdo Requiere tener habilitado la Librería PDO ');
if ( ! class_exists ('ClassBaseVNZ') ) die('<b>Error Fatal:</b> vnzsql_pdo Requiere incluir (class.basevnz.php) Ejemplo: require_once(\'vnzsql/base/class.basevnz.php\'); ');

/**
 * Componente PDO (VNZSQL database abstraction library)
 *
 * @package vnzsql_pdo
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

class vnzsql_pdo extends ClassBaseVNZ {

    /**
     * Instancia para el patrón de diseño singleton (instancia única)
     * @var object instancia
     * @access private
     */
    private static $instancia;

    /**
     * Define el tipo de respuesta para el componente PDO
     * @var string 
     * @access public
     */
    public $pdo_resptipo;
    
    /**
     * Método Constructor
     * @param string $prefResTipo
     */
    private function __construct($dbtipo = "NULL", $prefResTipo = VNZ_RES_ASSOC) {
        $this->dbtipo = $dbtipo;
        $this->prefResTipo = $prefResTipo;
    }

    /**
     * Método Destructor
     */
    public function __destruct() {
        $this->db = false;
    }

    /**
     * Realiza la instancia
     * @return object
     */
    public static function singleton() {
        if (!isset(self::$instancia)) {
            $clase = __class__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Envía el tipo de respuesta
     * @param string $prefResTipo
     */
    public function set_tiporespuesta($prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        switch ($this->prefResTipo) {
            case VNZ_RES_ASSOC:
                $this->pdo_resptipo = PDO::FETCH_ASSOC;
                break;
            case VNZ_RES_NUM:
                $this->pdo_resptipo = PDO::FETCH_NUM;
                break;
            case VNZ_RES_AMBAS:
                $this->pdo_resptipo = PDO::FETCH_BOTH;
                break;
            default:
                $this->pdo_resptipo = PDO::FETCH_ASSOC;
                break;
        }
    }

    /**
     * Conectar retorna true si conecto con éxito, o falso si no hay conexión
     * @param string $host
     * @param string $usuario
     * @param string $clave
     * @param string $basedatos
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host = '', $usuario = '', $clave = '', $basedatos = '', $puerto = 0, $persistente = FALSE) {
        parent::conectar($host, $usuario, $clave, $basedatos, $puerto, $persistente);
        $this->host = $host;
        $this->usuario = $usuario;
        $this->clave = $clave;
        $this->basedatos = $basedatos;
        $this->puerto = $puerto;
        $this->persistente = $persistente;
        
        $this->tipos_basedatos = array("sqlite2",
            "sqlite3", "sqlsrv", "mssql", "mysql",
            "postgres", "ibm", "dblib", "odbc", "oracle",
            "ifmx", "fbd");

        if (in_array($this->dbtipo, $this->tipos_basedatos)) {
            switch ($this->dbtipo) {
                case "mssql":
                    $this->db = new PDO("mssql:host=$host;dbname=$basedatos", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "sqlsrv":
                    //puerto por defecto = ?
                    $this->db = new PDO("sqlsrv:server=$host,$puerto;basedatos=$basedatos", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "ibm":
                    //puerto por defecto = ?
                    $this->db = new PDO("ibm:DRIVER={IBM DB2 ODBC DRIVER};basedatos=$basedatos; HOSTNAME=$host;puerto=$puerto;PROTOCOL=TCPIP;", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "dblib":
                    //puerto por defecto = 10060
                    $this->db = new PDO("dblib:host=$host:$puerto;dbname=$basedatos", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "odbc":
                    // recordar probar esto..
                    $this->db = new PDO("odbc:Driver={Microsoft Access Driver (*.mdb)};Dbq=C:\accounts.mdb;Uid=$usuario");
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "oracle":
                    $this->db = new PDO("OCI:dbname=$basedatos;charset=UTF-8", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "ifmx":
                    $this->db = new PDO("informix:DSN=InformixDB", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "fbd":
                    $this->db = new PDO("firebird:dbname=$host:$basedatos", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "mysql":
                    //puerto por defecto = 3306
                    $this->db = new PDO("mysql:host=$host;port=$puerto;dbname=$basedatos", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "sqlite2":
                    $this->db = new PDO("sqlite:" . $host);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "sqlite3":
                    $this->db = new PDO("sqlite:" . $host);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                case "postgres":
                    $this->db = new PDO("pgsql:dbname=$basedatos;port=$puerto;host=$host", $usuario, $clave);
                    if ($this->db != false) {
                        return true;
                    } else {
                        $this->error = "Error PDO FALSE";
                        return false;
                    }
                    break;
                default:
                    $this->error = "Tipo DB PDO incorrecto!";
                    return false;
            }
        }
    }
    
    /**
     * Envió el tipo de Base de datos
     * @param string $dbtipo
     */
    public function set_dbtipo($dbtipo) {
        $this->dbtipo = strtolower($dbtipo);
    }

    /**
     * Método Clone
     */
    public function __clone() {
        trigger_error('La clonación de este objeto no está permitida ', E_USER_ERROR);
    }

    /**
     * Método wakeup
     */
    public function __wakeup() {
        trigger_error("No puede deserializar una instancia de " . get_class($this) . " Class. ", E_USER_ERROR);
    }

    /**
     * Ejecuta las Consultas a la base de datos
     * @param string $query
     * @return boolean
     */
    public function query($query) {
        if ($this->db != null) {
            $this->ultimoQuery = $query;
            $this->resultado = $this->db->query($query);
            if ($this->resultado != false) {
                return true;
            } else {
                $this->error = "resultado false";
                return false;
            }
        }
        return false;
    }

    /**
     * Retorna el resultado de una consulta (Dependiendo mucho del tipo de resultado enviado Ver línea 155)
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_siguiente($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    return $this->resultado->fetch(PDO::FETCH_ASSOC);
                    break;
                case VNZ_RES_NUM:
                    return $this->resultado->fetch(PDO::FETCH_NUM);
                    break;
                case VNZ_RES_AMBAS:
                    //return $this->resultado->fetchAll(PDO::FETCH_BOTH);
                    return $this->resultado->fetch(PDO::FETCH_BOTH);
                    break;
                default:
                    $this->error = "Tipo de resultado incorrecto!";
                    return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retorna un resultado completo de una consulta
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_todo($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    return $this->resultado->fetchAll(PDO::FETCH_ASSOC);
                    break;
                case VNZ_RES_NUM:
                    return $this->resultado->fetchAll(PDO::FETCH_NUM);
                    break;
                case VNZ_RES_AMBAS:
                    return $this->resultado->fetchAll(PDO::FETCH_BOTH);
                    break;
                default:
                    $this->error = "Tipo de resultado incorrecto!";
                    return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retorna el número de rows afectados por una consulta
     * @return boolean|integer
     */
    public function rows_afectados() {
        if ($this->resultado != null) {
            return $this->resultado->rowCount();
            //$rows = $this->resultado->fetchAll();
            //return count($rows);
        } else {
            $this->error = "rows_afectados FALSE";
            return false;
        }
    }

    /**
     * Retorna el número de rows en una consulta
     * @return boolean|integer
     */
    public function numero_rows() {
        if ($this->resultado != null) {
            //return $this->resultado->rowCount();
            $rows = $this->resultado->fetchall();
            return count($rows);
        } else {
            $this->error = "numero_rows FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un objeto
     * @return boolean|object
     */
    public function fobject() {
        if ($this->resultado != null) {
            return $this->resultado->fetchObject();
        } else {
            $this->error = "fobject FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un único objeto
     * @param string $query
     * @return boolean|object
     */
    public function get_uno($query) {
        if($this->cache){
            return $this->query_cache($query,'get_uno',$this->prefResTipo);
        }
        $this->query($query);
        if ($this->resultado != null) {
            $row = $this->resultado->fetchAll(PDO::FETCH_NUM);
            $rs = (object) $row[0];
            return $rs;
        } else {
            $this->error = "get_uno query error";
            return false;
        }
    }

    /**
     * Retorna el nombre de las columnas
     * @return boolean|array
     */
    public function get_nombrecolumnas() {
        if ($this->resultado != null) {
            $numf = $this->resultado->columnCount();
            $array = array();
            for ($i = 0; $i < $numf; $i++) {
                $fnombre = $this->resultado->getColumnMeta($i);
                array_push($array, $fnombre['name']);
            }
            return $array;
        } else {
            $this->error = "GetColumnsName FALSE";
            return false;
        }
    }

    /**
     * Método get_database retorna los nombres de las bases de datos
     * @return boolean|array
     */
    public function get_database() {
        if($this->db != null){
            $ListaDbs = array();
            switch ($this->dbtipo) {
                case "sqlsrv":
                case "mssql":
                case "ibm":
                case "dblib":
                case "odbc":
                case "sqlite2":
                case "sqlite3":
                    $resultado = $this->db->query("SELECT name FROM sys.Databases");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaDbs, $row);
                    }
                    return $ListaDbs;
                case "oracle":
                    // Si da error o no retorna nada, prueba con esta línea comentada
                    //$resultado = $this->db->query("select * from user_tablespaces");
                    $resultado = $this->db->query("select * from v$this->basedatos");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaDbs, $row);
                    }
                    return $ListaDbs;
                case "ifmx":
                case "fbd":
                    return "No Implementado";
                case "mysql":
                    $resultado = $this->db->query("SHOW DATABASES");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaDbs, $row);
                    }
                    return $ListaDbs;
                case "postgres":
                    $resultado = $this->db->query("select datname as name from pg_database");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaDbs, $row);
                    }
                    return $ListaDbs;
                break;
            
                default:
                    $this->error = 'Comando Desconocido!!';
                    return FALSE;
                    break;
            }
            return $this->get_tablas();
        }else{
            $this->error = "get_database db FALSE";
            return false;
        }
    }
    
    /**
     * Método get_tablas retorna las tablas de una base de datos
     * @return boolean|array
     */
    public function get_tablas() {
        if($this->db != null){
            $ListaTablas = array();
            switch ($this->dbtipo) {
                case "sqlsrv":
                case "mssql":
                case "ibm":
                case "dblib":
                case "odbc":
                case "sqlite2":
                case "sqlite3":
                    $resultado = $this->db->query("select name from sysobjects where xtype='U'");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaTablas, $row);
                    }
                    return $ListaTablas;
                case "oracle":
                    $resultado = $this->db->query("SELECT table_name FROM cat");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaTablas, $row);
                    }
                    return $ListaTablas;
                case "ifmx":
                case "fbd":
                    $resultado = $this->db->query("SELECT RDB$RELATION_NAME FROM RDB$RELATIONS WHERE RDB$SYSTEM_FLAG = 0 AND RDB$VIEW_BLR IS NULL ORDER BY RDB$RELATION_NAME");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaTablas, $row);
                    }
                    return $ListaTablas;
                case "mysql":
                    $resultado = $this->db->query("show tables");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                       array_push($ListaTablas, $row);
                    }
                    return $ListaTablas;
                case "postgres":
                    $resultado = $this->db->query("select relname as name from pg_stat_user_tables order by relname");
                    while ($row = $resultado->fetch($this->pdo_resptipo)) {
                        array_push($ListaTablas, $row);
                    }
                    return $ListaTablas;
                break;
            
                default:
                    $this->error = 'Comando Desconocido!!';
                    return FALSE;
                    break;
            }
            return $this->get_columnas();
        }else{
            $this->error = "get_tablas db FALSE";
            return false;
        }
    }

    /**
     * Liberar el resultado de una consulta
     * @return boolean
     */
    public function liberar() {
        if ($this->resultado != null) {
            return $this->resultado->closeCursor();
        } else {
            $this->error = "liberar FALSE";
            return false;
        }
    }

    /**
     * Retorna una cadena Escapada
     * @param string $str
     * @return boolean|string
     */
    public function escape($str) {
        if ($this->db != false) {
            return $this->db->quote($str);
        } else {
            $this->error = "escape db FALSE";
            return false;
        }
    }

    /**
     * Realiza la Desconexión
     * @return boolean
     */
    public function desconectar() {
        if ($this->db =! null) {
            $this->_AntesDesconectar();
            $this->db = null;
            $this->resultado = null;
            return true;
        } else {
            return false;
        }
    }

}

?>