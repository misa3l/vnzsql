<?php
/* vnzsql/class.mysqlidb.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://gitlab.com/misa3l/vnzsql .
* Licenciado bajo la AGPL versión 3 o superior .
*/

if ( ! function_exists ('mysqli_connect') ) die('<b>Error Fatal:</b> vnzsql_mysqli Requiere tener habilitado la extensi&oacute;n mysqli_connect() ');
if ( ! class_exists ('ClassBaseVNZ') ) die('<b>Error Fatal:</b> vnzsql_mysqli Requiere incluir (class.basevnz.php) Ejemplo: require_once(\'vnzsql/base/class.basevnz.php\'); ');

/**
 * Componente MYSQLI (VNZSQL database abstraction library)
 *
 * @package vnzsql_mysqli
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

class vnzsql_mysqli extends ClassBaseVNZ {

    /**
     * Instancia para el patrón de diseño singleton (instancia única)
     * @var object instancia
     * @access private
     */
    private static $instancia;

    /**
     * Método Constructor
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo = "mysqli", $prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        $this->dbtipo = $dbtipo;
        $this->_id = "MYSQLI";
    }

    /**
     * Realiza la instancia
     * @return object
     */
    public static function singleton() {
        if (!isset(self::$instancia)) {
            $clase = __CLASS__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Método Clone
     */
    public function __clone() {
        trigger_error("La clonación de este objeto no está permitida ", E_USER_ERROR);
    }

    /**
     * Método wakeup
     */
    public function __wakeup() {
        trigger_error("No puede deserializar una instancia de " . get_class($this) . " Class. ", E_USER_ERROR);
    }

    /**
     * Método Destructor
     */
    public function __destruct() {
        $this->desconectar();
    }

    /**
     * Envía el tipo de respuesta
     * @param string $prefResTipo
     */
    public function set_tiporespuesta($prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
    }

    /**
     * Conectar retorna true si conecto con éxito, o falso si no hay conexión
     * @param string $host
     * @param string $usuario
     * @param string $clave
     * @param string $basedatos
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host, $usuario, $clave, $basedatos, $puerto = 3306, $persistente = FALSE) {
        parent::conectar($host, $usuario, $clave, $basedatos, $puerto, $persistente);
        $this->db = mysqli_connect($host, $usuario, $clave, $basedatos ,$puerto);
        if ($this->db) {
            if (mysqli_select_db($this->db, $basedatos)) {
                return TRUE;
            }
            $this->error = @mysqli_error($this->db);
        } else {
            return FALSE;
        }
        $this->error = "Error al conectar a Base de datos";
        return FALSE;
    }

    /**
     * Envía el charset a la conexión
     * @param string $charset
     * @return boolean
     */
    public function set_charset($charset) {
        parent::set_charset($charset);
        if ($this->db != null) {
            $rs = @mysqli_set_charset($this->db, $charset);
            if ($rs) {
                return true;
            } else {
                $this->error = "set_charset FALSE";
                return false;
            }
        } else {
            $this->error = "set_charset This DB FALSE";
            return false;
        }
    }

    /**
     * Ejecuta las Consultas a la base de datos
     * @param string $query
     * @return boolean
     */
    public function query($query) {
        parent::query($query);
        if ($this->db != null) {
            $this->resultado = @mysqli_query($this->db, $query);
            if ($this->resultado != false) {
                return true;
            } else {
                $this->error = @mysqli_error($this->db);
                return false;
            }
        }
        return false;
    }

    /**
     * Retorna el nombre de las columnas
     * @return boolean|array
     */
    public function get_nombrecolumnas() {
        parent::get_nombrecolumnas();
        if ($this->resultado != null) {
            $valores = mysqli_fetch_field($this->resultado);
            return (array) $valores;
        } else {
            $this->error = "get_nombrecolumnas FALSE";
            return false;
        }
    }

    /**
     * Retorna el resultado de una consulta (Dependiendo mucho del tipo de resultado enviado Ver línea 70)
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_siguiente($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    return @mysqli_fetch_array($this->resultado, MYSQLI_ASSOC);
                    break;
                case VNZ_RES_NUM:
                    return @mysqli_fetch_array($this->resultado, MYSQLI_NUM);
                    break;
                case VNZ_RES_AMBAS:
                    return @mysqli_fetch_array($this->resultado, MYSQLI_BOTH);
                    break;
                default:
                    $this->error = "Tipo de resultado incorrecto!";
                    return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Liberar el resultado de una consulta
     * @return boolean
     */
    public function liberar() {
        if ($this->resultado != null) {
            return @mysqli_free_result($this->resultado);
        } else {
            $this->error = "liberar FALSE";
            return false;
        }
    }

    /**
     * Retorna el número de rows en una consulta
     * @return boolean|integer
     */
    public function numero_rows() {
        if ($this->resultado != null) {
            return @mysqli_num_rows($this->resultado);
        } else {
            $this->error = "numero_rows FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un objeto
     * @return boolean|object
     */
    public function fobject() {
        parent::fobject();
        if ($this->resultado != null) {
            return @mysqli_fetch_object($this->resultado);
        } else {
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un único objeto
     * @param string $query
     * @return boolean|object
     */
    public function get_uno($query) {
        parent::get_uno($query);
        if($this->cache){
            return $this->query_cache($query,'get_uno',$this->prefResTipo);
        }
        $this->query($query);
        if ($this->resultado != NULL) {
            $row = @mysqli_fetch_object($this->resultado);
            return $row;
        } else {
            $this->error = "GetOne query error";
            return FALSE;
        }
    }

    /**
     * Retorna el número de rows afectados por una consulta
     * @return boolean|integer
     */
    public function rows_afectados() {
        if ($this->db != null) {
            return @mysqli_affected_rows($this->db);
        } else {
            return false;
        }
    }

    /**
     * Realiza la Desconexión
     * @return boolean
     */
    public function desconectar() {
        parent::desconectar();
        if ($this->db != null) {
            @mysqli_close($this->db);
            $this->_AntesDesconectar();
            return true;
        } else {
            $this->error = "No Conectado a ninguna Base de Datos!";
            return false;
        }
    }

}

?>